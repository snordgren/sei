# sei

Sei is a generic glue library that can be used to create spaced repetition
systems. By implementing the interfaces SpacingStrategy, Storage, TimeUnit,
and TimeUnitFactory, you can instantiate a Scheduler that manages scheduling
anything using the desired SpacingStrategy.
