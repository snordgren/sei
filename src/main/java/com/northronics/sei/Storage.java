package com.northronics.sei;

import java.util.List;

/**
 * Stores the information about the exercises.
 *
 * @author Silas Nordgren
 */
public interface Storage<G, S, T extends TimeUnit> {

	/**
	 * Access the current grade for the supplied object.
	 *
	 * @param scheduledObject The object for which to get the grade.
	 * @return The grade of the object.
	 */
	G getGrade(S scheduledObject);

	/**
	 * Access the next repeat time for the specific object. Should throw
	 * an IllegalArgumentException if the parameter does not have a
	 *
	 * @param scheduledObject The scheduled object.
	 * @return The
	 */
	T getRepeatTime(S scheduledObject);

	/**
	 * @param until The time to compare with. Any objects that are before this
	 * time should be returned.
	 * @return The scheduled objects that are due to be repeated.
	 */
	List<S> getRepeatable(T until);

	/**
	 * Get the number of exercises that are repeatable until the supplied time
	 * unit.
	 *
	 * @param until The time to check.
	 * @return The number of scheduled items that are due to be repeated until
	 * the supplied time unit.
	 */
	int getRepeatableCount(T until);

	/**
	 * Checks whether there is a time listed for the specific item.
	 *
	 * @param s The object to check.
	 * @return True if the object has a scheduled time for repetition.
	 */
	boolean hasRepeatTime(S s);

	/**
	 * Stores the scheduled object along with the time it should be retrieved
	 * next. If there is a previous time stored for the scheduled object, it
	 * should not be
	 *
	 * @param scheduledObject The scheduled object.
	 * @param time The time it should be repeated.
	 */
	void store(S scheduledObject, G grade, T time);
}
