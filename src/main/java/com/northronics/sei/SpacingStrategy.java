package com.northronics.sei;

/**
 * Interface defining how grades should translate into new time units.
 *
 * @param <G> The type to use for representing grades.
 * @param <T> The type to use for representing time.
 * @author Silas Nordgren
 */
public interface SpacingStrategy<G, T extends TimeUnit> {

	/**
	 * Get the time unit for the next time that an exercise of the supplied grade
	 * should be repeated.
	 *
	 * @param repetitionTime The time of repetition.
	 * @param grade The current grade that should be assessed.
	 * @return The time that should pass before the next repetition.
	 */
	T getNextTime(T repetitionTime, G grade);

}
