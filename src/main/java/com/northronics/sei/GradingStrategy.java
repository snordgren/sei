package com.northronics.sei;

/**
 * A GradingStrategy governs how the grade of a scheduled exercise changes upon
 * success or failure.
 *
 * @param <G> The type to use to represent grades.
 * @param <R> The type to use to represent results.
 * @author Silas Nordgren
 */
public interface GradingStrategy<G, R> {

	/**
	 * @return The first grade that all exercises are created with.
	 */
	G getInitialGrade();

	/**
	 * Computes the new grade for the exercise based on the current grade
	 * and the result of the latest repetition.
	 *
	 * @param currentGrade The current grade of the exercise.
	 * @param result The result of the latest repetition.
	 * @return The calculated new grade.
	 */
	G getNewGrade(G currentGrade, R result);
}
