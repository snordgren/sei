package com.northronics.sei;

import java.util.List;

/**
 * Primary scheduler which combines a grading strategy, a spacing strategy,
 * a storage implementation, and a time unit factory to generate the desired
 * spaced repetition system.
 *
 * @param <R> The type to use for representing the result of an exercise.
 * @param <S> The type to use for representing scheduled exercises.
 * @param <T> The type to use for representing time.
 * @author Silas Nordgren
 */
public class Scheduler<G, R, S, T extends TimeUnit<T>> {
	private final GradingStrategy<G, R> gradingStrategy;
	private final SpacingStrategy<G, T> spacingStrategy;
	private final Storage<G, S, T> storage;
	private final TimeUnitFactory<T> timeUnitFactory;

	/**
	 * Instantiates a new scheduler with its component parts.
	 *
	 * @param gradingStrategy The strategy to use for grading.
	 * @param spacingStrategy The strategy to use for spacing
	 * @param storage Solution for storing data about exercises.
	 * @param timeUnitFactory Factory for creating time units when needed.
	 */
	public Scheduler(GradingStrategy<G, R> gradingStrategy,
			SpacingStrategy<G, T> spacingStrategy,
			Storage<G, S, T> storage,
			TimeUnitFactory<T> timeUnitFactory) {
		this.gradingStrategy = gradingStrategy;
		this.spacingStrategy = spacingStrategy;
		this.storage = storage;
		this.timeUnitFactory = timeUnitFactory;
	}

	public void create(S scheduledObject) {
		storage.store(scheduledObject, gradingStrategy.getInitialGrade(),
				timeUnitFactory.now());
	}

	public S getNextExercise() {
		return getNextExercise(timeUnitFactory.now());
	}

	/**
	 * Accesses the next exercise.
	 *
	 * @param until The time to check.
	 * @return The next exercise to repeat.
	 */
	public S getNextExercise(T until) {
		List<S> repeats = storage.getRepeatable(until);
		if (repeats.size() == 0) {
			throw new IllegalArgumentException(
					"There were no repetitions before the supplied time.");
		} else if (repeats.size() == 1) {
			return repeats.get(0);
		} else {
			S earliest = repeats.get(0);
			T earliestRepeatTime = storage.getRepeatTime(earliest);
			for (int i = 1; i < repeats.size(); i++) {
				S current = repeats.get(i);
				T repeatTime = storage.getRepeatTime(current);
				if (earliestRepeatTime.isAfter(repeatTime)) {
					earliest = current;
					earliestRepeatTime = repeatTime;
				}
			}

			return earliest;
		}
	}

	/**
	 * Returns the number of repetitions that are due before or at the
	 * supplied time unit.
	 *
	 * @param until The time to check before.
	 * @return The number of items due for repetition.
	 */
	public int getRepeatCount(T until) {
		return storage.getRepeatableCount(until);
	}

	public SpacingStrategy<G, T> getSpacingStrategy() {
		return spacingStrategy;
	}

	public Storage getStorage() {
		return storage;
	}

	public TimeUnitFactory<T> getTimeUnitFactory() {
		return timeUnitFactory;
	}

	public boolean hasNextExercise() {
		return hasNextExercise(timeUnitFactory.now());
	}

	public boolean hasNextExercise(T until) {
		return storage.getRepeatableCount(until) > 0;
	}

	/**
	 * Updates the exercise with the result of the latest repetition.
	 * @param scheduledObject The exercise.
	 * @param result The result of the repetition.
	 */
	public void update(S scheduledObject, R result) {
		final G currentGrade = storage.getGrade(scheduledObject);
		final G newGrade = gradingStrategy.getNewGrade(currentGrade, result);
		final T now = timeUnitFactory.now();
		final T next = spacingStrategy.getNextTime(now, newGrade);
		storage.store(scheduledObject, newGrade, next);
	}
}
