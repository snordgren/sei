package com.northronics.sei;

/**
 * Factory for creating TimeUnit implementations to be used by the scheduler
 *
 * @param <T> The specific subclass of TimeUnit that should be used.
 * @author Silas Nordgren
 */
public interface TimeUnitFactory<T extends TimeUnit> {

	/**
	 * Create a new time unit representing the current time.
	 * @return The new time unit.
	 */
	T now();
}
