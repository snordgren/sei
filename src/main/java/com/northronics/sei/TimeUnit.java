package com.northronics.sei;

/**
 * Interface representing a unit of time.
 *
 * @param <T> The specific subclass to use.
 * @author Silas Nordgren
 */
public interface TimeUnit<T extends TimeUnit> {

	/**
	 * Checks whether this time unit is after the argument in the timeline.
	 *
	 * @param timeUnit The time unit to check.
	 * @return True if this time unit occurs later than the argument.
	 */
	boolean isAfter(T timeUnit);
}

