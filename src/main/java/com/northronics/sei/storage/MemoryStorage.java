package com.northronics.sei.storage;

import com.northronics.sei.Storage;
import com.northronics.sei.TimeUnit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * In-memory implementation of the Storage interface.
 *
 * @author Silas Nordgren
 */
public class MemoryStorage<G, S, T extends TimeUnit<T>> implements Storage<G, S, T> {
	private final Map<S, G> grades = new HashMap<S, G>();
	private final Map<S, T> repeatTimes = new HashMap<S, T>();

	@Override
	public G getGrade(S scheduledObject) {
		return grades.get(scheduledObject);
	}

	@Override
	public T getRepeatTime(S scheduledObject) {
		if (!repeatTimes.containsKey(scheduledObject)) {
			throw new IllegalArgumentException(scheduledObject + " has no repeat time.");
		}
		return repeatTimes.get(scheduledObject);
	}

	@Override
	public List<S> getRepeatable(T until) {
		List<S> repeatable = new ArrayList<S>();
		for (Map.Entry<S, T> entry : repeatTimes.entrySet()) {
			if (until.isAfter(entry.getValue())) {
				repeatable.add(entry.getKey());
			}
		}
		return repeatable;
	}

	@Override
	public int getRepeatableCount(T until) {
		int count = 0;
		for (Map.Entry<S, T> entry : repeatTimes.entrySet()) {
			if (until.isAfter(entry.getValue())) {
				count++;
			}
		}
		return count;
	}

	@Override
	public boolean hasRepeatTime(S s) {
		return repeatTimes.containsKey(s);
	}

	@Override
	public void store(S scheduledObject, G grade, T time) {
		grades.put(scheduledObject, grade);
		repeatTimes.put(scheduledObject, time);
	}
}
