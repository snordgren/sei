package com.northronics.sei.result;

/**
 * Represents the result scale that is used in the Anki program. Called
 * assessed because the user assesses their own result and the difficulty
 * they had.
 *
 * @author Silas Nordgren
 */
public enum AssessedResult {

	/**
	 * The answer was wrong, and should be repeated during the session.
	 */
	AGAIN,

	/**
	 * The answer was recalled correctly, but with some difficulty or
	 * hesitation.
	 */
	HARD,

	/**
	 * The answer was recalled correctly, but not perfectly.
	 */
	GOOD,

	/**
	 * The answer was perfectly easy to recall.
	 */
	EASY

}
