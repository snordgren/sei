package com.northronics.sei.result;

/**
 * The simplest form of result, only recording whether the answer was correct
 * or not.
 *
 * @author Silas Nordgren
 */
public enum BinaryResult {

	/**
	 * The answer was not incorrect.
	 */
	WRONG,

	/**
	 * The answer was correct.
	 */
	CORRECT;
}
