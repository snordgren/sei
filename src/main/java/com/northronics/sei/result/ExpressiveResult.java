package com.northronics.sei.result;

/**
 * Represents an assessed result as used in the article "Application of a
 * computer to improve the results obtained in working with the SuperMemo
 * method", by P. A. Wozniak.
 *
 * Called expressive because it contains a lot of information about the
 * particular result.
 *
 * @author Silas Nordgren
 */
public enum ExpressiveResult {

	/**
	 * Complete blackout. No recall.
	 */
	BLACKOUT,

	/**
	 * Incorrect response; the correct one remembered.
	 */
	INCORRECT_REMEMBERED,

	/**
	 * Incorrect response; where the correct one seemed easy to recall.
	 */
	INCORRECT_EASY_RECALL,

	/**
	 * Correct response recalled with serious difficulty.
	 */
	CORRECT_SERIOUS_DIFFICULTY,

	/**
	 * Correct response after a hesitation.
	 */
	CORRECT_HESITATION,

	/**
	 * Perfect recall.
	 */
	PERFECT
}
